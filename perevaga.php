<!-- Слайдер -->
	<div class="gidr-press-row-content-five">
		<div class="row gidr-press-row-content-five-one">
		
			<p><?php _e('Із вдячними замовниками ми стараємось підтримувати відносини.', irswood) ?></p>
			<p><?php _e('Адже головне джерело наших проектів - рекомендації задоволених клієнтів.', irswood) ?></p>
		</div>
		<div class="gidr-press-row-content-five-two">
			<div class="container container-padding-top">
				<div class="row">
					<div class="col-lg-4 colored"><p style="line-height: 34px;"><?php _e('Можливість низьковідсоткового кредитування', irswood) ?></p></div>
					<div class="col-lg-3 colored"><p><?php _e('Супровід на усіх етапах', irswood) ?></p></div>
					<div class="col-lg-4 colored"><p style="line-height: 34px;"><?php _e('ГАРАНТІЯ  навіть на вживане обладнання', irswood) ?></p></div>
				</div>
				<div class="row">
					<div class="col-lg-4 colored"><p><?php _e('Допомога в пошуку сировини', irswood) ?></p></div>
					<div class="col-lg-3 colored"><p><?php _e('Післягарантійне обслуговування', irswood) ?></p></div>
					<div class="col-lg-4 colored"><p><?php _e('Просування вашої продукції', irswood) ?></p></div>
				</div>
				<div class="row">
					<div class="col-lg-4 colored"><p><?php _e('Аудит лінії та сировини', irswood) ?></p></div>
					<div class="col-lg-3 colored"><p><?php _e('Відповідність технічній безпеці', irswood) ?></p></div>
					<div class="col-lg-4 colored"><p><?php _e('Допомога в оптимізація логістики', irswood) ?></p></div>
				</div>
				<div class="row">
					<div class="col-lg-4 colored"><p><?php _e('Консультація', irswood) ?></p></div>
					<div class="col-lg-3 colored"><p><?php _e('Навчання', irswood) ?></p></div>
					<div class="col-lg-4 colored"><p><?php _e('Шеф-монтаж', irswood) ?></p></div>
				</div>
				<div class="row">
					<div class="col-lg-12"><div class="col-lg-12 colored" style="flex: 0 0 100%;max-width: 100%;"><p><?php _e('Допомагаємо створити бізнес, який можна масштабувати.', irswood) ?></p></div></div>
				</div>
				<div style="width: 70%;margin: 10px auto;">
				<?php if(get_bloginfo('language')=='uk') {echo do_shortcode('[contact-form-7 id="86" title="White UK"]');} 
				else {echo do_shortcode('[contact-form-7 id="62" title="White RU"]');
			} ?>
				</div>
				<style type="text/css">	
	.call-form, form>p {
		text-align: center;
	}</style>
		</div>
	</div>
</div>
<!-- Слайдер -->