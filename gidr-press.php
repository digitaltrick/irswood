<?php
/*
 * Template name: gidr-press
 */
?>

<?php get_header('header.php'); ?>
	<div class="container-fluid gidr-press-bck">
		<div class="row gidr-press-row-content-one">
		<h1><?php _e('Гідравлічні преси для брикетів типів RUF та Nestro недорого від виробника. Швидке встановлення.', irswood) ?></h1>
				<p><?php _e('Гідравлічні брикетні преси - ком’ютеризований вид пресів, продукт яких найрентабельніший для бізнесу. Їхній головний недолік - тонкі, важкі програмні налаштування.', irswood) ?></p>	
			<div class="row gidr-press-row-content-one-one">
				<div class="col-lg-6 col-md-6">
					<p style="margin: 0;"><?php _e('Основні наші клієнти:', irswood) ?></p>
					<div class="row">
						<div class="col-lg-6 col-md-6">
							<ul>
								<li><?php _e('власники ліній', irswood) ?></li>
								<li><?php _e('інвестори', irswood) ?></li>
							</ul>
						</div>
						<div class="col-lg-6 col-md-6">
							<ul>
								<li><?php _e('власники біовідходів', irswood) ?></li>
								<li><?php _e('досвідчені інженери', irswood) ?></li>
							</ul>
						</div>
					</div>
					<p><?php _e('Продуктивність пресів: від 50 до 600 кг/год.', irswood) ?></p>
				</div>
				<div class="col-lg-6 col-md-6">
					<p style="margin: 0;"><?php _e('Обладнання Irswood працює!', irswood) ?></p>
					<ul>
						<li><?php _e('більше 200 пресів, встановлених у Європі та Україні', irswood) ?></li>
						<li><?php _e('більше 40 готових ліній брикетування “під ключ”', irswood) ?></li>
						<li><?php _e('0 випадків несправностей за Х років', irswood) ?></li>
						<li><?php _e('Гарантія навіть на б/у обладнання', irswood) ?></li>
					</ul>
				</div>
			</div>

			<?php get_template_part('press-page-icone-text-block'); ?>

			<div class="row gidr-press-row-content-one-two">
				<div class="col-lg-6"><h6 style="margin-top: 10px;"><?php _e('Отримайте налаштований гідравлічний прес зі зносостійкої шведської сталі на 20-80% дешевше, ніж в Європі.', irswood) ?></h6></div>
				<div class="call-form col-lg-6">
					<?php 
						if(get_bloginfo('language')=='uk') {echo do_shortcode('[contact-form-7 id="84" title="Black UK"]');} 
						else {echo do_shortcode('[contact-form-7 id="85" title="Black RU"]');} 
					?>
					<p><?php if(get_bloginfo('language')=='uk') {echo ('Зателефонуємо до 30 хвилин після заявки. Щодня з 9:00 до 21:00');} else {echo ('Перезвоним до 30 минут после заявки. Каждый день с 9:00 до 21:00.');
			} ?></p>
				</div>
			</div>
		</div>


		<div id="gidr-press-row-content-two" class="row gidr-press-row-content-two">
			<div class="col-lg-4">
				<img src="<?php echo get_template_directory_uri(); ?>/images/hydravlicpress.jpg" style="width: 100%; height: 100%;">
			</div>
			<div class="col-lg-8">
				<p><?php _e('Irswood організував роботу польського заводу-партнера, створив технологію виробництва комплектуючих для лінії і центр реставрації б/у обладнання. Таким чином, ви отримаєте 100% аналог оригінального обладнання за зниженою ціною, яке за Х років жодного разу не вийшло з ладу.', irswood) ?></p>
			</div>
			<div>
				<p><?php _e('Гідравлічні преси IrsWood та покроковий супровід дають можливість створити бізнес із мінімальним терміном окупності. Проте зауважте, що гідропреси вимагають тонких програмних налаштувань, що збільшує вимоги до кваліфікованості робітників. Окрім цього, вимоги до сировини дещо вищі ніж в ударно-механічних пресів.', irswood) ?></p>
				<p><?php _e('Ви можете порадитись із спеціалістами Irswood на тему цілей та потреб свого бізнесу.', irswood) ?></p>
			</div>
		</div>

		<div class="row gidr-press-row-content-three" style="display: none;">
			<div class="col-lg-6">
				<!-- <iframe width="100%" height="200" src="https://www.youtube.com/embed/3EIbWjkimAs" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe> -->
				<img src="gray-squared-wallpaper-background.jpg" style="width: 100%; height: 100%;">
			</div>
			<div class="col-lg-6">
				<!-- <iframe width="100%" height="200" src="https://www.youtube.com/embed/iRXJXaLV0n4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe> -->
				<img src="gray-squared-wallpaper-background.jpg" style="width: 100%; height: 100%;">
			</div>
		</div>

<!-- Слайдер тексту і фото-->
<div class="slideshow-container">

  <!-- Full-width images with number and caption text -->
  <div class="mySlides fade">
    <div class="row">
      <div class="col-lg-6"><img src="<?php echo get_template_directory_uri(); ?>/images/hydr-press.jpg" style="width:100%;padding-left:50px;"></div>
      <div class="text col-lg-6"><p><?php _e('Технічні характеристики пресу', irswood) ?></p>
	      	<p><?php _e('Серія пресу Nestro" NBV-150', irswood) ?></p>
			<p><?php _e('Продуктивність', irswood) ?></p>
			<p><?php _e('Енергоспоживання', irswood) ?></p>
			<p><?php _e('Вага пресу', irswood) ?> </p> 
			<p><?php _e('Персонал', irswood) ?></p>
			<p><?php _e('Діаметр брикета', irswood) ?></p>
		</div>
    </div>
  </div>

  <div class="mySlides fade">
    <div class="row">
      <div class="col-lg-6"><img src="<?php echo get_template_directory_uri(); ?>/images/hydr-press.jpg" style="width:100%;padding-left:50px;"></div>
      <div class="text col-lg-6">
      		<p><?php _e('Технічні характеристики пресу', irswood) ?></p>
	      	<p><?php _e('Серія пресу Nestro" NBV-150', irswood) ?></p>
			<p><?php _e('Продуктивність', irswood) ?></p>
			<p><?php _e('Енергоспоживання', irswood) ?></p>
			<p><?php _e('Габарити', irswood) ?></p>
			<p><?php _e('Вага пресу', irswood) ?></p> 
			<p><?php _e('Персонал', irswood) ?></p>
			<p><?php _e('Діаметр брикета', irswood) ?></p>
      </div>
    </div>
  </div>

  <div class="mySlides fade">
    <div class="row">
      <div class="col-lg-6"><img src="<?php echo get_template_directory_uri(); ?>/images/hydr-press.jpg" style="width:100%;padding-left:50px;"></div>
      <div class="text col-lg-6">
      		<p><?php _e('Технічні характеристики пресу', irswood) ?></p>
	      	<p><?php _e('Серія пресу Nestro" NBV-150', irswood) ?></p>
			<p><?php _e('Продуктивність', irswood) ?></p>
			<p><?php _e('Енергоспоживання', irswood) ?></p>
			<p><?php _e('Габарити', irswood) ?></p>
			<p><?php _e('Вага пресу', irswood) ?></p> 
			<p><?php _e('Персонал', irswood) ?></p>
			<p><?php _e('Діаметр брикета', irswood) ?></p>
		</div>
    </div>
  </div>

  <div class="mySlides fade">
    <div class="row">
      <div class="col-lg-6"><img src="<?php echo get_template_directory_uri(); ?>/images/hydr-press.jpg" style="width:100%;padding-left:50px;"></div>
      <div class="text col-lg-6">
	      	<p><?php _e('Технічні характеристики пресу', irswood) ?></p>
	      	<p><?php _e('Серія пресу Nestro" NBV-150', irswood) ?></p>
			<p><?php _e('Продуктивність', irswood) ?></p>
			<p><?php _e('Енергоспоживання', irswood) ?></p>
			<p><?php _e('Габарити', irswood) ?></p>
			<p><?php _e('Вага пресу', irswood) ?></p> 
			<p><?php _e('Персонал', irswood) ?></p>
			<p><?php _e('Діаметр брикета', irswood) ?></p>
      </div>
    </div>
  </div>

  <div class="mySlides fade">
    <div class="row">
      <div class="col-lg-6"><img src="<?php echo get_template_directory_uri(); ?>/images/hydr-press.jpg" style="width:100%;padding-left:50px;"></div>
      <div class="text col-lg-6"><p><?php _e('', irswood) ?>Технічні характеристики пресу</p>
	      	<p><?php _e('Технічні характеристики пресу', irswood) ?></p>
	      	<p><?php _e('Серія пресу Nestro" NBV-150', irswood) ?></p>
			<p><?php _e('Продуктивність', irswood) ?></p>
			<p><?php _e('Енергоспоживання', irswood) ?></p>
			<p><?php _e('Габарити', irswood) ?></p>
			<p><?php _e('Вага пресу', irswood) ?></p> 
			<p><?php _e('Персонал', irswood) ?></p>
			<p><?php _e('Діаметр брикета', irswood) ?></p>
		</div>
    </div>
  </div>


  <!-- Next and previous buttons -->
  <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
  <a class="next" onclick="plusSlides(1)">&#10095;</a>
</div>
<br>

<!-- The dots/circles -->
<div style="text-align:center">
  <span class="dot" onclick="currentSlide(1)"></span> 
  <span class="dot" onclick="currentSlide(2)"></span> 
  <span class="dot" onclick="currentSlide(3)"></span> 
  <span class="dot" onclick="currentSlide(4)"></span>
  <span class="dot" onclick="currentSlide(5)"></span>
</div>
  


  <script type="text/javascript">
    var slideIndex = 1;
showSlides(slideIndex);

// Next/previous controls
function plusSlides(n) {
  showSlides(slideIndex += n);
}

// Thumbnail image controls
function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1} 
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none"; 
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block"; 
  dots[slideIndex-1].className += " active";
}
  </script>

 <div class="gidr-press-row-content-four">
 	<strong><p><?php _e('Продуктивність наведена на прикладі тирси.', irswood) ?></p></strong>
 	<p><?php _e('Для того, аби визначити прес, також необхідно знати зольність та якісний склад (процентне співвідношення різних видів і порід) сировини. ', irswood) ?></p>
 	<p><?php _e('Якщо ви не знаєте як це з’ясувати - зателефонуйте спеціалістам Irswood чи замовте консультацію на сайті.', irswood) ?></p>
 	<div class="call-form">
		<!-- <form>
			<input type="text" name="phone" class="gidr-press-row-content-one-input white-background-button" placeholder="+ (380) 111-11-11" >
			<input type="submit" name="subscribe" value="<?php _e('Передзвоніть мені', irswood) ?>" class="gidr-press-row-content-one-two-submit">
		</form> -->
		<?php if(get_bloginfo('language')=='uk') {echo do_shortcode('[contact-form-7 id="86" title="White UK"]');} 
				else {echo do_shortcode('[contact-form-7 id="62" title="White RU"]');
			} ?>
		</div>
 	</div>
<!-- Слайдер -->
	<?php get_template_part('perevaga'); ?>
<!-- Слайдер -->

	</div>
<?php get_footer() ?>
