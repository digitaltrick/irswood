<footer>
	<div class="row footer">
		<div class="col-lg-3">
			<div class="row telephone"><img src="<?= get_template_directory_uri();?>/images/telephone.svg"> +380679614969</div>
			<!-- <div class="row viber"><img src="<?= get_template_directory_uri();?>/images/viber.svg"> @irswood </div>
			<div class="row skype"><img src="<?= get_template_directory_uri();?>/images/skype.svg"> irswood </div> -->	
		</div>
		<div class="col-lg-6">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2573.683662423295!2d23.957776815709003!3d49.82961047939451!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x473ae75ce7e1dd71%3A0x7d931c591a3396b0!2z0LLRg9C70LjRhtGPINCT0L7RgNC-0LTQvtGG0YzQutCwLCAyMTQsINCb0YzQstGW0LIsINCb0YzQstGW0LLRgdGM0LrQsCDQvtCx0LvQsNGB0YLRjCwgNzkwMDA!5e0!3m2!1suk!2sua!4v1513867883585" width="100%" height="200px" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>
		<div class="col-lg-3">
			<div class="row adresa"><img src="<?= get_template_directory_uri();?>/images/placeholder.svg"><p><?php _e('вул. Городоцька б. 214', irswood) ?></p>
			<p><?php _e('м. Львів, Україна', irswood) ?></p></div>
			<div class="row dig-trick">© 2018. Digital Trick</div>
		</div>
	</div>	
</footer>
</body>
  <?php wp_footer(); ?>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
</html>