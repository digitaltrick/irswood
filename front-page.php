<?php
/*
 * Template name: Main Page 
 */
?>
<?php get_header('header.php'); ?>
<div class="container-fluid first-content-block">
	<div class="row first-content-block-row-one">
		<div class="col-lg-12">
			<h1><?php _e('Обладнання для виробництва брикетів та пелетів. Лінії «під ключ»', irswood) ?></h1>
		</div>
	</div>

	<div class="row first-content-block-row-two">
		<div class="col-lg-7 ">
			<h3><?php _e('Супровід нових партнерів від заснування бізнесу до реалізації продукції.', irswood) ?></h3>
			<p><?php _e('Ми пропонуємо нове, відновлене і б/у обладнання з гарантією', irswood) ?></p>
			<p class="ne-vyishli-z-ladu" style="margin-bottom: 1rem;"><?php _e('Із <span>40</span> самостійно встановлених нами ліній за <span>20</span> років жодна не вийшла з ладу', irswood) ?></p>
			<div style="margin-top: 40px;">
				<p class="cta-p" style="font-size: 1.3rem;"><?php _e('Залиште номер щоб отримати консультацію - вона безкоштовна.', irswood) ?> <span class="warning"> <?php echo $error; ?></span></p>

			<!-- <form action="post.php" method="POST" class="form-tel" id="form-tel">
				<input type="tel" name="tel" id="tel" class="first-content-block-row-two-input" placeholder="(+380) 111-11-11" style="padding: 0 15px;">
				<input type="submit" name="subscribe" id="submit-id" value="<?php _e('Надіслати', irswood) ?>" class="first-content-block-row-two-form-submit">
			</form> -->
			<div class="call-form">
				<?php if(get_bloginfo('language')=='uk') {echo do_shortcode('[contact-form-7 id="63" title="MainPage"]');} 
				else {echo do_shortcode('[contact-form-7 id="91" title="Main RU"]');
			} ?>
					<p style="font-size: 1.2rem;"><?php if(get_bloginfo('language')=='uk') {echo ('Зателефонуємо до 30 хвилин після заявки. Щодня з 9:00 до 21:00');} else {echo ('Перезвоним до 30 минут после заявки. Каждый день с 9:00 до 21:00.');
			} ?></p>
			</div>

		</div>
		</div>

		<div class="col-lg-5">
			<div class="row" style="text-shadow: 0 0 15px rgba(0, 0, 0, 0.5);">
				<div class="col-lg-6 col-md-6  first-content-block-row-two-over-video"><p><?php _e('Кредит під низькі %', irswood) ?></p></div>
				<div class="col-lg-5 col-md-5  first-content-block-row-two-over-video" style=""><p><?php _e('Гарантія', irswood) ?></p></div>
			</div>
			<div class="row video-box">
				<iframe width="100%" height="300px" src="https://www.youtube.com/embed/5zo-KDY-uGo" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>	
			</div>
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 first-content-block-row-two-over-video"><p><?php _e('Безкоштовний аудит вашої лінії', irswood) ?></p></div>
			</div>
		</div>
	</div>
	<div class="row second-content-block-row-one">
		<div class="col-lg-12" style="text-align: center;"><h2><?php _e('Власна демонстративна лінія виробництва брикетів нестро. Показовий виїзд на встановлені лінії.', irswood) ?></h2>
		<h3><?php _e('Використовуємо вашу сировину для наочності.', irswood) ?></h3>
		</div>
	</div>

	<?php get_template_part('main-page-icone-text-block'); ?>

	<div class="row second-content-block-row-two" style="padding-top: 20px;"> 
		<div class="col-lg-12">
			<div class="tabs">
			    <input type="radio" name="inset" value="" id="tab_1" checked>
			    <label for="tab_1"><?php _e('Власникам біовідходів', irswood) ?></label>
			    <input type="radio" name="inset" value="" id="tab_2">
			    <label for="tab_2"><?php _e('Інвесторам', irswood) ?></label>
			    <input type="radio" name="inset" value="" id="tab_3">
			    <label for="tab_3"><?php _e('Власникам ліній', irswood) ?></label>
			    <div id="txt_1">
			       <p><?php _e('Установка лінії брикетування для виробництва євродрів найчастіше цікавить:', irswood) ?></p>
			       <ul>
			       <li><?php _e('Лісозаготівельні підприємства', irswood) ?></li>
			       <li><?php _e('Виробники алкогольних напоїв', irswood) ?></li>
			       <li><?php _e('Фермерські господарства', irswood) ?></li>
			       <li><?php _e('Сільські кооперативи та сільради', irswood) ?></li>
			       <li><?php _e('Торфовища', irswood) ?></li>
			       <li><?php _e('Власники біологічних відходів', irswood) ?></li>
			       </ul>
			       <p><?php _e('Також, Irswood пропонує преси-гранулятори для виробництва пелет. Гранули використовують лише як паливо, але й для тварин в якості гранульованих комбікормів, підстилок, пелетних наповнювачів для котів...', irswood) ?></p>
					<p><?php _e('Окрім заробітку на біовідходах, власників бізнесу приваблюють імідж еко-компанії, впевненість в рості ніші на протязі десятиліть і сухі цифри - рентабельність бізнесу на євродровах чи гранулах від 30-50%. А одне цехове виробництво біотоплива може виготовляти від 50 до 3000 кг продукції на годину, споживаючи при цьому від 50 кВт електроенергії.', irswood) ?></p>
					<p><?php _e('Лише фахівці можуть правильно підібрати обладнання для лінії виробництва еко палива.', irswood) ?></p>
					<p><?php _e('Наші спеціалісти безкоштовно допоможуть вам підібрати необхідне обладнання. Лише залиште номер.', irswood) ?></p>
					<?php 
						if(get_bloginfo('language')=='uk') {echo do_shortcode('[contact-form-7 id="89" title="green bioothody"]');} 
						else {echo do_shortcode('[contact-form-7 id="92" title="green bioothody ru"]');} 
					?>
			    </div>
			    <div id="txt_2">
			        <p><?php _e('Як європейські інвестори, так і представники середнього класу, які бажають вигідно вкласти кошти, все частіше обирають паливний бізнес з переробки відходів. Ринок збуту брикетів і пелет є перспективним, так як:', irswood) ?></p>
			        <ul>
					<li><?php _e('інвестиції в пелетний бізнес, як і в брикетувальники, окупають себе в короткі терміни', irswood) ?></li>
					<li><?php _e('рентабельність - від 30-50%', irswood) ?></li>
					<li><?php _e('ліцензія не потрібна', irswood) ?></li>
					<li><?php _e('біопаливо відновлюване, а отже - вкрай екологічне', irswood) ?></li>
					<li><?php _e('на цьому ринку попит в рази перевищує пропозицію завдяки широкому діапазону потенційних клієнтів - від власників мангалів і домашніх пічок до ТЕЦ та великих заводів', irswood) ?></li>
					</ul>	
					<p><?php _e('Проте перед тим, як відкрити бізнес з виробництва пелет або брикетів, слід знати про існуючі в ньому підводні камені:', irswood) ?></p>
					<ul>
					<li><?php _e('визначення необхідного рівня продуктивності лінії', irswood) ?></li>
					<li><?php _e('підготовка приміщення', irswood) ?></li>
					<li><?php _e('налаштування лінії', irswood) ?></li>
					<li><?php _e('пожежна безпека', irswood) ?></li>
					</ul>
					<p><?php _e('Маючи досвід роботи на цьому ринку більше 20 років, ми розробили ефективні рішення для кожного з цих завдань. Тому рентабельність бізнесу з виробництва пелет та брикетів для наших клієнтів складає від 30% вже в перші роки роботи. А стабільний прибуток в подальшому гарантує високу якість встановлених Irswood пресів для виробництва брикетів. Жодна з 40 поставлених нами ліній не вийшла з ладу.', irswood) ?></p>
					<p><?php _e('Інвестуйте в людей, а не тільки в вид бізнесу.', irswood) ?></p>
					<p><?php _e('Дізнайтесь детальніше, чому саме цей бізнес виграшний.', irswood) ?></p>
					<p><?php _e('Прочитайте статтю або одразу залиште свій номер - і тоді ви дізнаєтесь більше зі слів засновників Irswood.', irswood) ?></p>

			        <?php 
						if(get_bloginfo('language')=='uk') {echo do_shortcode('[contact-form-7 id="89" title="green investor"]');} 
						else {echo do_shortcode('[contact-form-7 id="93" title="green investor ru"]');} 
					?>
			        <input type="submit" name="tabs-submit-link" value="<?php _e('Про Irswood', irswood) ?>" class="tabs-submit-link" href="#txt_2" disabled="disabled">
			        
			    </div>
			    <div id="txt_3">
			       <p><?php _e('На жаль, рівень надання послуг деякими конкурентами не може претендувати на задовільний. Тому, нам часто доводиться займатися обслуговуванням і ремонтом вже готових ліній брикетування та гранулювання.', irswood) ?></p>
					<p><?php _e('Це цілий спектр послуг, в які входять:', irswood) ?></p>
					<ul>
					<li><?php _e('аудит лінії і матеріалу', irswood) ?></li>
					<li><?php _e('налаштування лінії брикетування та гранулювання', irswood) ?></li>
					<li><?php _e('заходи щодо підвищення пожежної безпеки', irswood) ?></li>
					</ul>
					<p><?php _e('… а також робота з самим обладнанням:', irswood) ?></p>
					<ul>
					<li><?php _e('апгрейд лінії', irswood) ?></li>
					<li><?php _e('автоматизація', irswood) ?></li>
					<li><?php _e('масштабування виробництва', irswood) ?></li>
					<li><?php _e('ремонт та заміна деталей', irswood) ?></li>
					</ul>
					<p><?php _e('Хочете вивести виробництво на новий рівень? Отримайте консультацію від Irswood, щоб закінчити ремонт і почати бізнес.', irswood) ?></p>
					<?php 
						if(get_bloginfo('language')=='uk') {echo do_shortcode('[contact-form-7 id="90" title="green linii"]');} 
						else {echo do_shortcode('[contact-form-7 id="94" title="green linii ru"]');} 
					?>			       
			    </div>
			</div>
		</div>
	</div>
</div>
<?php get_footer() ?>	