	<div class="row midle-text-icone-forma-one">
		<h4><?php _e('Обладнання Irswood розраховане на сировини в усіх формах:', irswood) ?></h4>
		<div class="row midle-text-icone-forma-one-one">			
			<div class="col-lg-6">
				<div class="row">
					<div class="col-lg-3 icon"><img src="<?php echo get_template_directory_uri(); ?>/images/log.svg"></div>
					<div class="col-lg-9"><p><?php _e('Деревина твердих та м’ягких порід: від берези та соснових до бука та дубу', irswood) ?></p></div>
				</div>	
			</div>
			<div class="col-lg-6">
				<div class="row">
					<div class="col-lg-3 icon"><img src="<?php echo get_template_directory_uri(); ?>/images/grass.svg"></div>
					<div class="col-lg-9"><p><?php _e('Інші трави: очерет, листя, коноплі, чай, люцерна, льон, тощо', irswood) ?></p></div>
				</div>
			</div>
		</div>
		<div class="row midle-text-icone-forma-one-two">
			<div class="col-lg-6">
				<div class="row">
					<div class="col-lg-3 icon"><img src="<?php echo get_template_directory_uri(); ?>/images/seeds.svg"></div>
					<div class="col-lg-9"><p><?php _e('Злакові: солома з пшениці, ячміню, жита, вівса, тощо, також висівки', irswood) ?></p></div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="row">
					<div class="col-lg-3 icon"><img src="<?php echo get_template_directory_uri(); ?>/images/Group.svg"></div>
					<div class="col-lg-9"><p><?php _e('Вугілля, торф', irswood) ?></p></div>
				</div>
			</div>
		</div>
		<div class="row midle-text-icone-forma-one-three">
			<div class="col-lg-6">
				<div class="row">
					<div class="col-lg-3 icon"><img src="<?php echo get_template_directory_uri(); ?>/images/cereal_muesli.svg"></div>
					<div class="col-lg-9"><p><?php _e('Лушпиння: соняшника, рису, мигдалю, грецького горіху, гірчиці, гречки, тощо', irswood) ?></p></div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="row">
					<div class="col-lg-3 icon"><img src="<?php echo get_template_directory_uri(); ?>/images/hops.svg"></div>
					<div class="col-lg-9"><p><?php _e('Відходи алкогольного виробництва, як пивна дробина', irswood) ?></p></div>
				</div>
			</div>
		</div>
	</div>
	