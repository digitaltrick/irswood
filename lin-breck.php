<?php
/*
 * Template name: lin-breck
 */
?>
<?php get_header('header.php'); ?>
<div class="container-fluid">
	<div class="row lin-breck-row-content-one">
		<div><h1><?php _e('Лінії для виробництва паливних брикетів із рослинних відходів та торфу в Європі', irswood) ?></h1></div>
		<div class="col-lg-6" style="padding-left: 0">
			<p><?php _e('Irswood - єдина компанія в регіоні, що самостійно встановлює та налаштовує лінії брикетування.', irswood) ?></p>
			<!-- <input  type="submit" class="how-btn" name="how-btn" value="<?php _e('Як це сталось?', irswood) ?>" disabled="disabled"> -->
			<p><?php _e('Ми пропонуємо лінії брикетування потужністю від 50 до 3000 кг/год для виготовлення брикетів нестро, pini key та RUF.', irswood) ?></p>
			<p><?php _e('Irswood - це не експерементальні розробки, а перевірена часом комбінована технологія виробництва.', irswood) ?></p>
			<p><?php _e('За Х років 0 несправних ліній із У встановлених.', irswood) ?></p>
		</div>
		<div class="col-lg-6" style="padding-left: 10%">
			<div style="margin-bottom: 1rem;"><img src="<?php echo get_template_directory_uri(); ?>/images/lin-breck-two.jpg" style="width: 100%;"></div>
			<div>
				<?php if(get_bloginfo('language')=='uk') {echo do_shortcode('[contact-form-7 id="96" title="lin-breck ua"]');} 
				else {echo do_shortcode('[contact-form-7 id="95" title="lin-breck ru"]');
			} ?>	

			</div>
		</div>
		<div><p><?php _e('Нажаль, в Україні немає виробників якісних пресів. Тому, ми беремо брендові преси європейських партнерів, а решту лінії комплектуємо чи виготовляємо під потреби замовника.', irswood) ?></p></div>
	</div>

	<div class="row lin-breck-row-content-two">
		
	</div>
<!-- СЛАЙДЕР ТЕКСТУ ПОЧАТОК -->
	<div class="lin-breck-row-content-three">
	<div class="lin-breck-row-content-three-one"><h3><?php _e('Irswood розробив технології виробництва для всіх видів пресів.', irswood) ?></h3></div>
		<div class="tabs-lin">
			    <input type="radio" name="inset" value="" id="tab_1-lin" checked>
			    <label for="tab_1-lin"><?php _e('Ударно-механічні', irswood) ?></label>

			    <input type="radio" name="inset" value="" id="tab_2-lin">
			    <label for="tab_2-lin"><?php _e('Гідравлічні', irswood) ?></label>

			    <input type="radio" name="inset" value="" id="tab_3-lin">
			    <label for="tab_3-lin"><?php _e('Екструдери/шнекові', irswood) ?></label>

			    <div id="txt_1-lin">
			    <div class="row">
			      		<div class="col-lg-4"><img src="<?php echo get_template_directory_uri(); ?>/images/udar-meh-lin.jpg" style="height: 100%; width: 100%;"></div>
				      	<div class="col-lg-8">
					    	<h4><?php _e('Ірсвуд рекомендує ударно-механічні преси.', irswood) ?></h4>
					      	<p><?php _e('Отримавши досвід роботи на різних брикетувальниках, ми виділили ударно-механічні преси як найбільш вигідні. Його застосовують великі підприємства, заводи та підприємці, що хочуть отримати стабільний і гарантований прибуток на виготовлені паливних брикетів. На відміну від гідравлічних пресів, вони не потребують тонких програмних налаштувань і, якщо їх правильно підібрати і запустити, можуть виготовляти паливні брикети 24/7. Пресування на механічному обладнанні відбувається за рахунок ударної сили. Вона потребує мінімального споживання струму і при цьому має високий рівень ККД.', irswood) ?></p>
				      	</div>
			      	</div>
			      	<div>
			      		<p><?php _e('Ударно-механічний прес може працювати майже без перерв, видаючи велику кількість готової продукції на добу. Проста технологія роботи дозволяє довго використовувати таке обладнання без зносу та зі збереженням високого рівня продуктивності. Крім того, продукція на виході має хороші споживчі характеристики:', irswood) ?></p>
			      			<ul>
			      				<li><?php _e('достатня щільність брикетів', irswood) ?></li>
			      				<li><?php _e('1-1,2 кг/дм3 - тривале горіння;', irswood) ?>
			      				<li><?php _e('міцність і стійкість при тривалому зберіганні.', irswood) ?></li>
			      			</ul>
			      			<a href="<?php _e('/uk/udarno-mehanіchnij-pres-dlja-briketіv/#udar-meh-press-row-content-two', irswood) ?>"><?php _e('Читати більше...', irswood) ?></a>
			      	</div>
			    </div>
			    <div id="txt_2-lin">
			    <div  class="row">
				      	<div class="col-lg-4"><img src="<?php echo get_template_directory_uri(); ?>/images/hydravlicpress.jpg" style="height: 100%; width: 100%;"></div>
				      	<div class="col-lg-8">
					    	<h4><?php _e('Виходячи з особливостей гідравлічного пресу, ми рекомендуємо його своїм клієнтам лише в окремих випадках.', irswood) ?></h4>
								<p><?php _e('Таке обладнання для виготовлення паливних брикетів підходить лише для малих підприємств та особистого використання. Це зумовлено рівнем його продуктивності, який залежить від особливостей технології. На відміну від ударно-механічного, цей брикетувальник потребує довгих перерв в роботі та працює лише 7-8 годин на день. Перевагою такого обладнання є його низька ціна.', irswood) ?> </p>							
				      	</div>
				      	<p><?php _e('За допомогою гідравлічного пресу виготовляють брикети типу руф - євродрова з високими експлуатаційними характеристиками, які високо оцінюються на європейському ринку. Серед їх недоліків можна виділити необхідність у спеціальних умовах зберігання (герметична упаковка), швидке вбирання вологи, низьку щільність (до 1 кг/дм3) та відносно нетривале горіння.', irswood) ?></p>
				      	<a href="<?php _e('/uk/gіdravlіchnij-pres-dlja-briketіv/#gidr-press-row-content-two', irswood) ?>"><?php _e('Читати більше...', irswood) ?></a>
			      	</div>			   
			    </div>
			    <div id="txt_3-lin">
			    <div class="row">
				   	<div class="col-lg-4"><img src="<?php echo get_template_directory_uri(); ?>/images/press_briket_shnek.jpg" style="height: 100%; width: 100%;"></div>
						    <div class="col-lg-8">
						      	<h4><?php _e('Ми можемо зайнятись установкою шнекового пресу за бажанням клієнта, проте радимо звернути увагу на більш якісні моделі.', irswood) ?></h4>
						      	<p><?php _e('Шнековий, або екструдерний прес зазвичай обирають люди, котрі не розбираються у сфері брикетування та не бачать явної різниці між видами пресів. В результаті вони отримують брикетувальник, який попри низьку ціну обходиться дуже дорого в експлуатації. Шнековий прес споживає багато електроенергії, має низький рівень пожежної безпеки через високі температури роботи та швидко зношується. Шнек потрібно міняти кожні 30 тонн вхідного матеріалу. Ця технологія заборонена в ЄС по ряду причин, які ми також радимо враховувати:', irswood) ?></p>
					      	</div>
					      	<ul>
					      		<li><?php _e('при роботі виділяється велика кількість чадного диму', irswood) ?></li>
					      		<li><?php _e('надмірний шум', irswood) ?></li>
					      		<li><?php _e('багато пилюки', irswood) ?></li>
					      	</ul>
					      	<p><?php _e('Єдина причина купити і встановити такий брикетувальник - це якість продукції на виході. Брикети Піні-Кей, що виготовляються за цією технологією, мають найкращі споживчі властивості. Їх щільність сягає 1,4 кг/дм3, відповідно вони добре і повільно горять.', irswood) ?></p>
					      		<a href="<?php _e('/uk/pres-ekstruder-dlja-vigotovlennja-briketіv/#ekst-press-row-content-two', irswood) ?>"><?php _e('Читати більше...', irswood) ?></a>
				    </div>
			    </div>
			</div>
	</div>
	<!-- СЛАЙДЕР ТЕКСТУ КІНЕЦЬ -->
	<div class=" row lin-breck-row-content-four">
		<div class="lin-breck-row-content-four-one">
			<h4 style="font-size: 1.4rem;"><?php _e('Успішно експлуатуємо власну лінію виробництва брикетів на основі ударно-механічного пресу.', irswood) ?></h4>
			<div class="row">
				<div class="col-lg-7">
					<!-- ТУТ БУДУТЬ ФОТОГРАФІЇ -->
					<!-- <p>ТУТ БУДУТЬ ФОТОГРАФІЇ</p> -->
				</div>
				<div class="col-lg-5">
					<img src="<?php echo get_template_directory_uri(); ?>/images/lin-breck-four.jpg" style="width: 100%;">
				</div>
			</div>
		</div>
		<div class="lin-breck-row-content-four-two">
			<div class="row">
				<div class="col-lg-7 lin-breck-row-content-four-two-one">
					<p><?php _e('Дрібні посередники, що часто маскуються під “конкурентів”, не беруться за великі проектoи - вони не можуть запустити якісну лінію брикетування. Irswood демонструє свою реальність на прикладі Микулинецького пивзаводу.', irswood) ?></p>
				</div>
				<div class="col-lg-5 lin-breck-row-content-four-two-two">
					<p><?php _e('Микулинецький завод', irswood) ?></p>
					<img src="<?php echo get_template_directory_uri(); ?>/images/lin-breck-three.jpg" style="width: 100%;">
				</div>
			</div>
		</div>
	</div>
	
	<!-- Слайдер -->
	<?php get_template_part('perevaga'); ?>
<!-- Слайдер -->

</div>
<?php get_footer() ?>