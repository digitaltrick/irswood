<?php
/*
 * Template name: Extrud
 */
?>
<?php get_header('header.php'); ?>
<div class="container-fluid">
		<div class="row ekst-press-row-content-one">
			<div class="ekst-press-row-content-one-one">
				<h1><?php _e('Екструдерний прес для брикетів Pini-Key недорого від виробника. Швидке встановлення.', irswood) ?></h1>
				<p><?php _e('Продуктивність пресів: 300 - 400 кг/год.', irswood) ?></p>
				<p><?php _e('Шнекові преси - дешеве обладнання, що виробляє брикети з високими споживчими якостями. Екструдерні преси заборонені в Європі через велику к-ть диму, що вимагає виробництва у нежитловому приміщенні. Як наслідок заборони, брикети піні кей є дефіцитним, рентабельним товаром при належному виробництві.', irswood) ?></p>
				<p><?php _e('Нові, реставровані та б/у шнекові преси із гарантією та “під ключ” за цінами виробника.', irswood) ?></p>
				<div style="padding-top: 1rem;" class="row">
				<div class="col-lg-6">
				<p style="font-size: 1.7rem; margin: 0;"><?php _e('Дешевий сир у мишоловці', irswood) ?></p>
				<p><?php _e('Отримайте повну інструкцію по нюансах роботи з пресами-екструдерами.', irswood) ?></p>
				</div>
				<div class="call-form col-lg-6">
					<!-- <form>
						<input type="text" name="phone" class="gidr-press-row-content-one-input" placeholder="+ (380) 111-11-11">
						<input type="submit" name="subscribe" value="<?php _e('Передзвоніть мені', irswood) ?>" class="gidr-press-row-content-one-two-submit">
					</form> -->
					<?php if(get_bloginfo('language')=='uk') {echo do_shortcode('[contact-form-7 id="84" title="Black UK"]');} 
				else {echo do_shortcode('[contact-form-7 id="85" title="Black RU"]');
			} ?>
			<p><?php if(get_bloginfo('language')=='uk') {echo ('Зателефонуємо до 30 хвилин після заявки. Щодня з 9:00 до 21:00');} else {echo ('Перезвоним до 30 минут после заявки. Каждый день с 9:00 до 21:00.');
			} ?></p>
				</div>
				</div>
			</div>				
		</div>

<!-- СЛАЙДЕР ПОЧАТОК -->		
			
<div class="slideshow-container" style="padding: 50px 0px;">

  <!-- Full-width images with number and caption text -->
  <div class="mySlides fade">
    <div class="row">
      <div class="col-lg-6"><img src="<?php echo get_template_directory_uri(); ?>/images/press_briket_shnek.jpg" style="width:100%;padding-left:20%"></div>
      <div class="text col-lg-6"><p><?php _e('Технічні характеристики пресу', irswood) ?></p>
	      	<p><?php _e('Серія пресу Nestro" NBV-150', irswood) ?></p>
			<p><?php _e('Продуктивність', irswood) ?></p>
			<p><?php _e('Енергоспоживання', irswood) ?></p>
			<p><?php _e('Габарити', irswood) ?></p>
			<p><?php _e('Вага пресу', irswood) ?></p> 
			<p><?php _e('Персонал', irswood) ?></p>
			<p><?php _e('Діаметр брикета', irswood) ?> </p>
		</div>
    </div>
  </div>

  <div class="mySlides fade">
    <div class="row">
      <div class="col-lg-6"><img src="<?php echo get_template_directory_uri(); ?>/images/press_briket_shnek.jpg" style="width:100%;padding-left:20%"></div>
      <div class="text col-lg-6">
      		<p><?php _e('Технічні характеристики пресу', irswood) ?></p>
	      	<p><?php _e('Серія пресу Nestro" NBV-150', irswood) ?></p>
			<p><?php _e('Продуктивність', irswood) ?></p>
			<p><?php _e('Енергоспоживання', irswood) ?></p>
			<p><?php _e('Габарити', irswood) ?></p>
			<p><?php _e('Вага пресу', irswood) ?></p> 
			<p><?php _e('Персонал', irswood) ?></p>
			<p><?php _e('Діаметр брикета', irswood) ?> </p>
      </div>
    </div>
  </div>

  <div class="mySlides fade">
    <div class="row">
      <div class="col-lg-6"><img src="<?php echo get_template_directory_uri(); ?>/images/press_briket_shnek.jpg" style="width:100%;padding-left:20%"></div>
      <div class="text col-lg-6">
      		<p><?php _e('Технічні характеристики пресу', irswood) ?></p>
	      	<p><?php _e('Серія пресу Nestro" NBV-150', irswood) ?></p>
			<p><?php _e('Продуктивність', irswood) ?></p>
			<p><?php _e('Енергоспоживання', irswood) ?></p>
			<p><?php _e('Габарити', irswood) ?></p>
			<p><?php _e('Вага пресу', irswood) ?></p> 
			<p><?php _e('Персонал', irswood) ?></p>
			<p><?php _e('Діаметр брикета', irswood) ?> </p>
		</div>
    </div>
  </div>

  <div class="mySlides fade">
    <div class="row">
      <div class="col-lg-6"><img src="<?php echo get_template_directory_uri(); ?>/images/press_briket_shnek.jpg" style="width:100%;padding-left:20%"></div>
      <div class="text col-lg-6">
	      	<p><?php _e('Технічні характеристики пресу', irswood) ?></p>
	      	<p><?php _e('Серія пресу Nestro" NBV-150', irswood) ?></p>
			<p><?php _e('Продуктивність', irswood) ?></p>
			<p><?php _e('Енергоспоживання', irswood) ?></p>
			<p><?php _e('Габарити', irswood) ?></p>
			<p><?php _e('Вага пресу', irswood) ?></p> 
			<p><?php _e('Персонал', irswood) ?></p>
			<p><?php _e('Діаметр брикета', irswood) ?> </p>
      </div>
    </div>
  </div>

  <div class="mySlides fade">
    <div class="row">
      <div class="col-lg-6"><img src="<?php echo get_template_directory_uri(); ?>/images/press_briket_shnek.jpg" style="width:100%;padding-left:20%"></div>
      <div class="text col-lg-6">
      		<p><?php _e('Технічні характеристики пресу', irswood) ?></p>
	      	<p><?php _e('Серія пресу Nestro" NBV-150', irswood) ?></p>
			<p><?php _e('Продуктивність', irswood) ?></p>
			<p><?php _e('Енергоспоживання', irswood) ?></p>
			<p><?php _e('Габарити', irswood) ?></p>
			<p><?php _e('Вага пресу', irswood) ?></p> 
			<p><?php _e('Персонал', irswood) ?></p>
			<p><?php _e('Діаметр брикета', irswood) ?> </p>
		</div>
    </div>
  </div>


  <!-- Next and previous buttons -->
  <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
  <a class="next" onclick="plusSlides(1)">&#10095;</a>
</div>
<br>

<!-- The dots/circles -->
<div style="text-align:center">
  <span class="dot" onclick="currentSlide(1)"></span> 
  <span class="dot" onclick="currentSlide(2)"></span> 
  <span class="dot" onclick="currentSlide(3)"></span> 
  <span class="dot" onclick="currentSlide(4)"></span>
  <span class="dot" onclick="currentSlide(5)"></span>
</div>
  


  <script type="text/javascript">
    var slideIndex = 1;
showSlides(slideIndex);

// Next/previous controls
function plusSlides(n) {
  showSlides(slideIndex += n);
}

// Thumbnail image controls
function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1} 
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none"; 
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block"; 
  dots[slideIndex-1].className += " active";
}
  </script>

<div id="ekst-press-row-content-two" class="row ekst-press-row-content-two">
  	<p><strong><?php _e('Продуктивність наведена на прикладі тирси.', irswood) ?></strong></p>
  	<p><?php _e('Для того, аби визначити потрібний прес, також необхідно знати зольність та якісний склад (процентне співвідношення різних видів і порід) сировини. Якщо ви не знаєте як це з’ясувати - зателефонуйте спеціалістам Irswood чи замовте консультацію на сайті.', irswood) ?></p>
</div>
<!-- СЛАЙДЕР КІНЕЦЬ -->
		
<div class="row ekst-press-row-content-three">
	<div class="col-lg-6"><img src="<?php echo get_template_directory_uri(); ?>/images/shnek-press-one.jpg" style="width: 100%;"></div>
	<div class="col-lg-6"><p><?php _e('Екструдерні преси вимагають періодичного ремонту та заміни обладнання, дотримання інструкцій використання. IrsWood, аби вирішити ці проблеми, окрім гарантійного та післягарантійного обслуговування, пропонує постачання деталей та постійний супровід. Головне для роботи із шнековим пресом - стежити та вкладатись у процес виробництва, адже попит на брикети перевищує пропозицію.', irswood) ?></p>
	<p><?php _e('Ударно-механічні преси володіють простішою технологією виробництва, та менше зусиль вимагають у догляді. Але вони підійдуть вам якщо ви більше знайомі та цікавитесь налагоджуванням ринку продажу. ', irswood) ?></p>
	</div>

	<p style="margin-top: 1rem;"><?php _e('Причини, котрі роблять роботу із екструдерними пресами вигідною:  ', irswood) ?></p>
	<p><?php _e('По-перше, брикети Pini-Key володіють найвищими споживчими характеристиками. Висока щільність та наскрізний отвір підвищують калорійність, температуру горіння та товарний вигляд, через що їх купують для опалення приватних будинків, камінів та мангалень.', irswood) ?></p>
	<p><?php _e('По-друге - вільний ринок збуту на ЄС. Це зумовлене тим, що через виділення диму при виробництві екструдери заборонені для встановлення у Європі, але високий рівень якості продукції змушує клієнтів купувати.', irswood) ?></p>
	<p><?php _e('Зверніться до IrsWood, щоб дізнатись всі деталі про преси-екструдери ще до угоди.', irswood) ?></p>
	<div class="ekst-press-send-form call-form">
					<!-- <form>
						<input type="text" name="phone" class="gidr-press-row-content-one-input white-background-button" placeholder="+ (380) 111-11-11">
						<input type="submit" name="subscribe" value="<?php _e('Передзвоніть мені', irswood) ?>" class="gidr-press-row-content-one-two-submit">
					</form> -->
				<?php if(get_bloginfo('language')=='uk') {echo do_shortcode('[contact-form-7 id="86" title="White UK"]');} 
				else {echo do_shortcode('[contact-form-7 id="62" title="White RU"]');
			} ?>

	</div>
</div>
<!-- Слайдер -->
	<?php get_template_part('perevaga'); ?>
<!-- Слайдер -->


</div>

<?php get_footer() ?>
