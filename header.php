<!DOCTYPE html>
<html>
<head>
	<title><?php echo wp_get_document_title(); ?></title>
<!--   <meta name="description" content="<?php _e('Прес для брикетів чи гранулятор? Лінії під ключ від Irswood укомплектовані обладнанням для виготовлення пелет і брикетів RUF, Pini Kay и Nestro.', irswood); ?>">  -->
	<meta charset="utf-8">
  <link rel="shortcut icon" type="image/png" href="<?= get_template_directory_uri();?>/images/favicon.png"/>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
  <link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet">

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5HZ2XL5');</script>
<!-- End Google Tag Manager -->

  <?php wp_head() ;?>
</head>
<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HZ2XL5"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


<header class="row" id="header"> 
  <h6 class="col-lg-1 col-md-1 col-sm-2 col-xs-2 logo"><a href="<? echo home_url('/', 'https'); ?>" style="text-decoration: none;color: white;">IrsWood</a></h6> 
  <p class="col-lg-6 col-md-6 col-sm-4 col-xs-4"><?php _e('Самостійно встановили понад 40 комбінованих ліній виробництва', irswood); ?></p> 
  <p class="col-lg-3 col-md-3 col-sm-2 col-xs-2"><?php _e('Тисячі реалізованих проектів', irswood) ?></p> 
  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 header-tel"><span>+380679614969</span>
  <ul style="position: absolute; right: 30px; display: flex;list-style-type: none;"><?php pll_the_languages(); ?></ul></div>
</header>
<nav class="navbar navbar-expand-lg navbar-light" id="nav">
  <?php wp_nav_menu(); ?>
</nav>
