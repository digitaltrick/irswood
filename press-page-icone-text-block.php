<div class="row icons-syrovyna">
	<div class="col-lg-4 ">
		<div class="row" style="margin: 1px;">
			<div class="col-lg-6 col-md-6 col-sm-6">
				<div class="row first-page-syr">
					<div class="col-lg-3 col-md-2 icon icon-fp"><img src="<?php echo get_template_directory_uri(); ?>/images/log.svg"></div>
					<div class="col-lg-9 col-md-10 fp-syr-text"><p><?php _e('Деревина твердих та м’ягких порід: від берези та соснових до бука та дубу', irswood) ?></p></div>
				</div>	
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6">
				<div class="row first-page-syr">
					<div class="col-lg-3 col-md-2 icon icon-fp"><img src="<?php echo get_template_directory_uri(); ?>/images/grass.svg"></div>
					<div class="col-lg-9 col-md-10 fp-syr-text"><p><?php _e('Інші трави: очерет, листя, коноплі, чай, люцерна, льон, тощо', irswood) ?></p></div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-4">
		<div class="row" style="margin: 1px;">
			<div class="col-lg-6 col-md-6 col-sm-6">
				<div class="row first-page-syr">
					<div class="col-lg-3 col-md-2 icon icon-fp"><img src="<?php echo get_template_directory_uri(); ?>/images/seeds.svg"></div>
					<div class="col-lg-9 col-md-10 fp-syr-text"><p><?php _e('Злакові: солома з пшениці, ячміню, жита, вівса, тощо, також висівки', irswood) ?></p>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6">
				<div class="row first-page-syr" style="height: 100%; width: 100%;">
					<div class="col-lg-3 col-md-2 icon icon-fp"><img src="<?php echo get_template_directory_uri(); ?>/images/Group.svg"></div>
					<div class="col-lg-9 col-md-10 fp-syr-text"><p><?php _e('Вугілля, торф', irswood) ?></p></div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-4">
		<div class="col-lg-6 col-md-6 col-sm-6">
			<div class="row first-page-syr">
					<div class="col-lg-3 col-md-2 icon icon-fp"><img src="<?php echo get_template_directory_uri(); ?>/images/cereal_muesli.svg"></div>
					<div class="col-lg-9 col-md-10 fp-syr-text"><p><?php _e('Лушпиння: соняшника, рису, мигдалю, горіху, гірчиці, гречки, тощо', irswood) ?></p></div>
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6">
			<div class="row first-page-syr">
				<div class="col-lg-3 col-md-2 icon icon-fp"><img src="<?php echo get_template_directory_uri(); ?>/images/hops.svg"></div>
				<div class="col-lg-9 col-md-10 fp-syr-text"><p><?php _e('Відходи алкогольного виробництва, як пивна дробина', irswood) ?></p></div>
			</div>
		</div>
	</div>
</div>