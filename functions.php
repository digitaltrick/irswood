<?php

function my_theme_setup(){
 load_theme_textdomain('irswood', get_template_directory() . '/languages'); 
 }
add_action('after_setup_theme', 'my_theme_setup'); 

function irswood_scripts() {
    wp_enqueue_style( 'style-css', get_stylesheet_uri() );
}
add_action( 'wp_enqueue_scripts', 'irswood_scripts' );

register_nav_menus(array(
      'primary' => __('Primary Menu')
    ));
?>