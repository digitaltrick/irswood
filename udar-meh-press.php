<?php
/*
 * Template name: Udar-meh-press
 */
?>
<?php get_header('header.php'); ?>
	<div class="container-fluid gidr-press-bck">
		<div class="row udar-press-row-content-one">
		<h1><?php _e('Ударно-механічні преси для брикетів Nestro із біовідходів недорого від виробника. Швидке встановлення.', irswood) ?></h1>
			<div class="row gidr-press-row-content-one-one">
				<div class="col-lg-6 col-md-6">
					<p style="margin: 0;"><?php _e('Основні наші клієнти:', irswood) ?></p>
					<div class="row">
						<div class="col-lg-6 col-md-6">
							<ul>
								<li><?php _e('власники ліній', irswood) ?></li>
								<li><?php _e('інвестори', irswood) ?></li>
							</ul>
						</div>
						<div class="col-lg-6 col-md-6">
							<ul>
								<li><?php _e('власники біовідходів', irswood) ?></li>
								<li><?php _e('досвідчені інженери', irswood) ?></li>
							</ul>
						</div>
					</div>
					<p><?php _e('Продуктивність пресів: від 100 до 1000 кг/год.', irswood) ?></p>
				</div>
				<div class="col-lg-6 col-md-6">
					<p style="margin: 0;"><?php _e('Обладнання Irswood працює!', irswood) ?></p>
					<ul>
						<li><?php _e('більше 200 пресів, встановлених у Європі та Україні', irswood) ?></li>
						<li><?php _e('більше 40 готових ліній брикетування “під ключ”', irswood) ?></li>
						<li><?php _e('0 випадків несправностей за Х років', irswood) ?></li>
						<li><?php _e('Гарантія навіть на б/у обладнання', irswood) ?></li>
					</ul>
				</div>
			</div>
			<?php get_template_part('press-page-icone-text-block'); ?>
			<div class="row udar-press-row-content-one-two">
				<div class="col-lg-6"><h6><?php _e('Створіть стабільний бізнес із виготовлення якісних брикетів 24/7 завдяки пресам Irswood із зносостійкої шведської сталі.', irswood) ?></h6></div>
				<div class="call-form col-lg-6">
					<?php if(get_bloginfo('language')=='uk') {echo do_shortcode('[contact-form-7 id="84" title="Black UK"]');} 
				else {echo do_shortcode('[contact-form-7 id="85" title="Black RU"]');
			} ?>
					<p><?php if(get_bloginfo('language')=='uk') {echo ('Зателефонуємо до 30 хвилин після заявки. Щодня з 9:00 до 21:00');} else {echo ('Перезвоним до 30 минут после заявки. Каждый день с 9:00 до 21:00.');
			} ?></p>
				</div>
			</div>
		</div>


		<div id="udar-meh-press-row-content-two" class="row gidr-press-row-content-two">
			<div class="col-lg-4">
				<!-- <iframe width="100%" height="200px" src="https://www.youtube.com/embed/1demxrg1pXE" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe> -->
				<img src="<?php echo get_template_directory_uri(); ?>/images/udar-meh-lin.jpg" style="width: 100%;height: 100%;">
			</div>
			<div class="col-lg-8">
				<p><?php _e('Irswood організував роботу польського заводу-партнера, створив технологію виробництва комплектуючих для лінії і центр реставрації б/у обладнання. Таким чином, ви отримаєте 100% аналог оригінального обладнання за зниженою ціною, яке за Х років жодного разу не вийшло з ладу.', irswood) ?></p>
			</div>
			<div>
				<p><?php _e('Irswood рекомендує обрати саме ударно-механічні преси для переробки біовідходів. Брикетуючі механічні преси - це найкращий вибір для початківців і власників невеликого бізнесу. На відміну від гідравлічних пресів, вони не потребують тонких програмних налаштувань і, якщо їх правильно підібрати і запустити, можуть виготовляти паливні брикети 24/7.', irswood) ?></p>
				<p><?php _e('У цьому ми пересвідчились не лише завдяки багатому досвіду, але й на власній лінії виробництва біопалива Nestro.', irswood) ?></p>
			</div>
		</div>

		<div class="row gidr-press-row-content-three" style="display: none;">
			<div class="col-lg-6">
				<!-- <iframe width="100%" height="200" src="https://www.youtube.com/embed/3EIbWjkimAs" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe> -->
				<img src="<?php echo get_template_directory_uri(); ?>/images/ydarnik_new.gif" style="width: 100%; height: 100%;">
			</div>
			<div class="col-lg-6">
				<!-- <iframe width="100%" height="200" src="https://www.youtube.com/embed/iRXJXaLV0n4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe> -->
				<img src="<?php echo get_template_directory_uri(); ?>/images/ydarnik_new.gif" style="width: 100%; height: 100%;">
			</div>
		</div>

<!-- Слайдер тексту і фото-->
<div class="slideshow-container">

  <!-- Full-width images with number and caption text -->
  <div class="mySlides fade">
    <div class="row">
      <div class="col-lg-6"><img src="<?php echo get_template_directory_uri(); ?>/images/ydarnik_new.gif" style="width:100%;padding-left:50px;"></div>
      <div class="text col-lg-6">
	      	<h5><?php _e('Технічні характеристики пресу:', irswood) ?></h5>
	      	<p><?php _e('Серія пресу Nestro" BT-150', irswood) ?></p>
			<p><?php _e('Продуктивність', irswood) ?></p>
			<p><?php _e('Енергоспоживання', irswood) ?></p>
			<p><?php _e('Габарити', irswood) ?></p>
			<p><?php _e('Вага пресу', irswood) ?></p> 
			<p><?php _e('Персонал', irswood) ?></p>
			<p><?php _e('Діаметр брикета', irswood) ?></p>
			<h5><?php _e('Вимоги до сировини', irswood) ?></h5>
			<p><?php _e('фракційний склад', irswood) ?></p>
			<p><?php _e('вологість 8-15%', irswood) ?></p>
		</div>
    </div>
  </div>

  <div class="mySlides fade">
    <div class="row">
      <div class="col-lg-6"><img src="<?php echo get_template_directory_uri(); ?>/images/ydarnik_new.gif" style="width:100%;padding-left:50px;"></div>
      <div class="text col-lg-6">
      		<h5><?php _e('Технічні характеристики пресу:', irswood) ?></h5>
	      	<p><?php _e('Серія пресу Nestro" BT-300', irswood) ?></p>
			<p><?php _e('Продуктивність', irswood) ?></p>
			<p><?php _e('Енергоспоживання', irswood) ?></p>
			<p><?php _e('Габарити', irswood) ?></p>
			<p><?php _e('Вага пресу', irswood) ?></p> 
			<p><?php _e('Персонал', irswood) ?></p>
			<p><?php _e('Діаметр брикета', irswood) ?></p>
			<h5><?php _e('Вимоги до сировини', irswood) ?></h5>
			<p><?php _e('фракційний склад', irswood) ?></p>
			<p><?php _e('вологість 8-15%', irswood) ?></p>
      </div>
    </div>
  </div>

  <div class="mySlides fade">
    <div class="row">
      <div class="col-lg-6"><img src="<?php echo get_template_directory_uri(); ?>/images/ydarnik_new.gif" style="width:100%;padding-left:50px;"></div>
      <div class="text col-lg-6">
	      	<h5><?php _e('Технічні характеристики пресу:', irswood) ?></h5>
			<p><?php _e('Серія пресу ВТ-500', irswood) ?></p>
			<p><?php _e('Продуктивність до 550 кг/год', irswood) ?></p>
			<p><?php _e('Енергоспоживання 20кВт', irswood) ?></p>
			<p><?php _e('Габарити', irswood) ?></p>
			<p><?php _e('Вага пресу 2250 кг', irswood) ?></p>
			<p><?php _e('Персонал', irswood) ?></p>
			<p><?php _e('Діаметр брикета 60мм', irswood) ?></p>
			<h5><?php _e('Вимоги до сировини', irswood) ?></h5>
			<p><?php _e('фракційний склад', irswood) ?></p>
			<p><?php _e('вологість 8-15%', irswood) ?></p>
		</div>
    </div>
  </div>

  <div class="mySlides fade">
    <div class="row">
      <div class="col-lg-6"><img src="<?php echo get_template_directory_uri(); ?>/images/ydarnik_new.gif" style="width:100%;padding-left:50px;"></div>
      <div class="text col-lg-6">
	      	<h5><?php _e('Технічні характеристики пресу:', irswood) ?></h5>
			<p><?php _e('Серія пресу ВТ-600', irswood) ?></p>
			<p><?php _e('Продуктивність 450 - 600 кг/год', irswood) ?></p>
			<p><?php _e('Енергоспоживання 22кВт', irswood) ?></p>
			<p><?php _e('Габарити', irswood) ?></p>
			<p><?php _e('Вага пресу 2300 кг', irswood) ?></p>
			<p><?php _e('Персонал', irswood) ?></p>
			<p><?php _e('Діаметр брикета 60мм', irswood) ?></p>
			<h5><?php _e('Вимоги до сировини', irswood) ?></h5>
			<p><?php _e('фракційний склад', irswood) ?></p>
			<p><?php _e('вологість 8-15%', irswood) ?></p>
      </div>
    </div>
  </div>

 <div class="mySlides fade">
    <div class="row">
      <div class="col-lg-6"><img src="<?php echo get_template_directory_uri(); ?>/images/ydarnik_new.gif" style="width:100%;padding-left:50px;"></div>
      <div class="text col-lg-6">
			<h5><?php _e('Технічні характеристики пресу:', irswood) ?></h5>
			<p><?php _e('Серія пресу ВТ-700', irswood) ?></p>
			<p><?php _e('Продуктивність 600-750 кг/год', irswood) ?></p>
			<p><?php _e('Енергоспоживання 25кВт', irswood) ?></p>
			<p><?php _e('Габарити', irswood) ?></p>
			<p><?php _e('Вага пресу 2900 кг', irswood) ?></p>
			<p><?php _e('Персонал', irswood) ?></p>
			<p><?php _e('Діаметр брикета 60мм', irswood) ?></p>
			<h5><?php _e('Вимоги до сировини', irswood) ?></h5>
			<p><?php _e('фракційний склад', irswood) ?></p>
			<p><?php _e('вологість 8-15%', irswood) ?></p>
		</div>
    </div>
  </div>

 <div class="mySlides fade">
    <div class="row">
      <div class="col-lg-6"><img src="<?php echo get_template_directory_uri(); ?>/images/ydarnik_new.gif" style="width:100%;padding-left:50px;"></div>
      <div class="text col-lg-6">
			<h5><?php _e('Технічні характеристики пресів:', irswood) ?></h5>
			<p><?php _e('Серія пресу ВТ-800', irswood) ?></p>
			<p><?php _e('Продуктивність 700-900 кг/год', irswood) ?></p>
			<p><?php _e('Енергоспоживання 37 кВт', irswood) ?></p>
			<p><?php _e('Габарити', irswood) ?></p>
			<p><?php _e('Вага пресу 3200 кг', irswood) ?></p>
			<p><?php _e('Персонал', irswood) ?></p>
			<p><?php _e('Діаметр брикета: 70 мм', irswood) ?></p>
			<h5><?php _e('Вимоги до сировини', irswood) ?></h5>
			<p><?php _e('фракційний склад', irswood) ?></p>
			<p><?php _e('вологість 8-15%', irswood) ?></p>
		</div>
    </div>
  </div>


  <!-- Next and previous buttons -->
  <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
  <a class="next" onclick="plusSlides(1)">&#10095;</a>
</div>
<br>

<!-- The dots/circles -->
<div style="text-align:center">
  <span class="dot" onclick="currentSlide(1)"></span> 
  <span class="dot" onclick="currentSlide(2)"></span> 
  <span class="dot" onclick="currentSlide(3)"></span> 
  <span class="dot" onclick="currentSlide(4)"></span>
  <span class="dot" onclick="currentSlide(5)"></span>
  <span class="dot" onclick="currentSlide(6)"></span>
</div>
  


  <script type="text/javascript">
    var slideIndex = 1;
showSlides(slideIndex);

// Next/previous controls
function plusSlides(n) {
  showSlides(slideIndex += n);
}

// Thumbnail image controls
function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1} 
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none"; 
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block"; 
  dots[slideIndex-1].className += " active";
}
  </script>

 <div class="gidr-press-row-content-four">
 	<strong><p><?php _e('Продуктивність наведена на прикладі тирси.', irswood) ?></p></strong>
 	<p><?php _e('Для того, аби визначити прес, також необхідно знати зольність та якісний склад (процентне співвідношення різних видів і порід) сировини.', irswood) ?></p>
 	<p><?php _e('Якщо ви не знаєте як це з’ясувати - зателефонуйте спеціалістам Irswood чи замовте консультацію на сайті', irswood) ?></p>
 	<div class="call-form">
		<?php if(get_bloginfo('language')=='uk') {echo do_shortcode('[contact-form-7 id="86" title="White UK"]');} 
				else {echo do_shortcode('[contact-form-7 id="62" title="White RU"]');
			} ?>
	</div>
 	</div>
 	
 	<?php get_template_part('perevaga'); ?>

	</div>
<?php get_footer() ?>
